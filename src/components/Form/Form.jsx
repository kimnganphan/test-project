import "./Form.scss";
import Button from "../Button/Button";
import { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addInfo,
  editInfo,
  replaceInfo,
  setInputChange,
} from "../../store/information/information.slice";
import { errorInp, generateRandomId } from "../../util/form.helper";

const inpArr = [
  {
    id: 1,
    label: "Linh Vuc Cha Id",
    type: "text",
    placeholder: "Enter ...",
    name: "LinhVucChaId",
    // onChange: (e) => handleInputOnChange(e),
    // value: state.inputChange?.LinhVucChaId
    //   ? state.inputChange?.LinhVucChaId
    //   : "",
  },
  {
    id: 2,
    label: "Ma Dinh Danh",
    type: "text",
    placeholder: "Enter ...",
    name: "MaDinhDanh",
    // onChange: (e) => handleInputOnChange(e),
    // value: state.inputChange?.MaDinhDanh
    //   ? state.inputChange?.MaDinhDanh
    //   : "",
  },
  {
    id: 3,
    label: "Ma Quan Ly Quoc Gia",
    type: "text",
    placeholder: "Enter ...",
    name: "MaQuanLy_QuocGia",
    // onChange: (e) => handleInputOnChange(e),
    // value: state.inputChange?.MaQuanLy_QuocGia
    //   ? state.inputChange?.MaQuanLy_QuocGia
    //   : "",
  },
  {
    id: 4,
    label: "Ten",
    type: "text",
    placeholder: "Enter ...",
    name: "Ten",
    // onChange: (e) => handleInputOnChange(e),
    // value: state.inputChange?.Ten ? state.inputChange?.Ten : "",
  },
  {
    id: 5,
    label: "Thu Tu",
    type: "text",
    placeholder: "Enter ...",
    name: "ThuTu",
    // onChange: (e) => handleInputOnChange(e),
    // value: state.inputChange?.ThuTu ? state.inputChange?.ThuTu : "",
  },
];

/**
 * form component that render input, label and 2 buttons and back function
 * @param {*} mode, setMode: set mode of the entire page to determine which button will show up (edit or add)
 * @returns input, label and 2 buttons (refresh and edit / add), back (switch from edit to add mode)
 */
const Form = ({ mode, setMode }) => {
  const [agree, setAgree] = useState(false);

  const dispatch = useDispatch();
  const state = useSelector((state) => state.info);

  const handleRefresh = () => {
    setMode("add");
    setAgree(false);
    dispatch(setInputChange({}));
  };

  const handleAdd = () => {
    const isValidated = validateForm();
    if (isValidated) {
      const Id = generateRandomId();
      //const Id = 2583;
      const inputObj = { ...state.inputChange, Id };

      const found = state.displayArr.find((x) => {
        return x.Id === inputObj.Id;
      });

      console.log(found);
      if (found) {
        dispatch(replaceInfo(inputObj));
      } else {
        dispatch(addInfo(inputObj));
      }
      dispatch(setInputChange({}));
      setAgree(false);
    }
  };

  const handleEdit = () => {
    const isValidated = validateForm();
    if (isValidated) {
      dispatch(editInfo({ id: state.inputChange.Id, info: state.inputChange }));
      dispatch(setInputChange({}));
      setAgree(false);
      setMode("add");
    }
  };

  const validateForm = () => {
    if (errorInp(state.inputChange?.LinhVucChaId)) {
      alert("Please Enter Linh Vuc Cha Id");
      return false;
    }
    if (errorInp(state.inputChange?.MaDinhDanh)) {
      alert("Please Enter Ma Dinh Danh");
      return false;
    }
    if (errorInp(state.inputChange?.MaQuanLy_QuocGia)) {
      alert("Please Enter Ma Quan Ly Quoc Gia");
      return false;
    }
    if (errorInp(state.inputChange?.Ten)) {
      alert("Please Enter Ten");
      return false;
    }
    if (errorInp(state.inputChange?.ThuTu)) {
      alert("Please Enter ThuTu");
      return false;
    }

    if (!agree) {
      alert("Please choose agree with terms");
      return false;
    }
    return true;
  };

  useEffect(() => {
    if (state.displayArr?.length === 0) {
      setAgree(false);
    }
  }, [state.displayArr?.length]);

  useEffect(() => {
    if (mode === "edit") {
      setAgree(false);
    }
  }, [mode]);
  const handleInputOnChange = (e) => {
    dispatch(
      setInputChange({
        ...state.inputChange,
        [e.target.name]: e.target.value,
      })
    );
  };

  return (
    <div className="formContainer">
      <div className="form-title">INFORMATION FORM</div>

      <div className="form-content">
        {inpArr.map((inp) => {
          return (
            <div className="form-content-item" key={inp.id}>
              <label>{inp.label}</label>
              <input
                type={inp.text}
                placeholder={inp.placeholder}
                name={inp.name}
                onChange={handleInputOnChange}
                // onChange: (e) => handleInputOnChange(e),
                // value={inp.value}
                value={state.inputChange[inp.name] ?? ""}
              />
            </div>
          );
        })}

        <div className="form-content-item-term">
          <input
            type="checkbox"
            name="agree"
            onChange={(e) => setAgree(e.target.checked)}
            checked={agree}
          />
          <label>I agree with the terms</label>
        </div>
      </div>

      <div className="form-buttons">
        <Button name={`Refresh`} color={`light`} onClick={handleRefresh} />
        {mode === "add" ? (
          <Button
            name={`Add`}
            color={`light`}
            disable={false}
            onClick={handleAdd}
          />
        ) : (
          <>
            <Button
              name={`Edit`}
              color={`dark`}
              disable={false}
              onClick={handleEdit}
            />
          </>
        )}
      </div>
    </div>
  );
};

export default Form;
