import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteAllInfo,
  deleteInfo,
  getInfoFromApi,
  getSingleInfo,
  overrideInfo,
  setInputChange,
} from "../../store/information/information.slice";
import Button from "../Button/Button";

import "./Table.scss";

/**
 * table component that render 4 buttons on top and a table contents information
 * @param {*} cols, data: take data from tableConstant
 * limit, count: control the information display on table
 * setMode: setMode of the entire page (2 mode: edit and add)
 * @returns table and 4 buttons (edit, delete, sync and deleteAll)
 */
const Table = ({ cols, data, setMode, mode }) => {
  const state = useSelector((state) => state.info);
  const dispatch = useDispatch();

  const [disableEdit, setDisableEdit] = useState(true);
  const [disableDelete, setDisableDelete] = useState(true);
  const [checkedArr, setCheckedArr] = useState([]);
  const [checkedAll, setCheckedAll] = useState(false);
  const [id, setId] = useState();
  const url =
    "https://tthckhapi.azurewebsites.net/api/v1/LinhVucs/GetTree?page=1&pageSize=10&sort=CreatedOn+DESC";

  /**
   * xử lý sửa
   */
  const handleEditClick = () => {
    //  set page mode to edit --> the ADD button in FORM component change into EDIT button and display BACK button which switch back into Add mode
    setMode("edit");
    // Edit button receives [id] get from checkbox and pass into redux to get exact information if the ID exist in array.
    dispatch(getSingleInfo(id));
  };

  /**
   * checkedArr: contents the ID of each row been checked
   * function 1: use forEach method to find information with Id in array and then use the result Id pass into redux to delete
   * function 2: after delete successfully, reset checkedArr
   * function 3:  disable Delete Button
   */

  const handleDeleteClick = () => {
    // checkedArr.forEach((item) => {
    //   const result =
    //     state.displayArr[state.displayArr.findIndex((inf) => inf.Id === item)];
    //   dispatch(deleteInfo(result.Id));
    // });
    const result = state.displayArr.filter((x) => {
      return checkedArr.includes(x.Id);
    });
    result.map((e) => {
      return dispatch(deleteInfo(e.Id));
    });

    setCheckedArr([]);
    setDisableDelete(true);
    setCheckedAll(false);
    setMode("add");

    dispatch(setInputChange({}));
  };

  const handleSyncClick = async () => {
    try {
      const response = await axios.get(url);
      const limitArr = [...response.data.DanhSachLinhVuc.slice(0, 10)];
      const resultArr = limitArr.map((x) => x.Id);
      const result = state.addNewArr.filter((ele) => {
        return resultArr.includes(ele.Id);
      });
      // new Map(
      //   [...state.addNewArr, ...limitArr].map((item) => [
      //     item.Id,
      //     item,
      //   ])
      // ,
      // new Map([...state.addNewArr, limitArr]).values();
      if (result.length > 0) {
        result.forEach((x) => {
          dispatch(overrideInfo(x.Id));
        });
        dispatch(getInfoFromApi(limitArr));
      } else {
        dispatch(getInfoFromApi(limitArr));
      }
      dispatch(setInputChange({}));
      setCheckedArr([]);
      setMode("add");
    } catch (error) {
      console.log(error);
    }
  };

  const handleDeleteAllClick = () => {
    dispatch(deleteAllInfo());
    setCheckedArr([]);
    setCheckedAll(false);
    setMode("add");
  };

  /**
   * function handle check single checkbox 
   * @param {*} e : event of checkbox
   * @param {*} id : ID of each row
   * function 1: if that row is checked, take the ID of the row and pass into state and pass the ID into arraySelected 
   * function 2: to check if the row checked or not

   */
  const handleChecked = (e, id) => {
    if (e.target.checked) {
      setId(Number(e.target.value));
    }

    setCheckedArr((prev) => {
      const isChecked = checkedArr.includes(id);

      if (isChecked) {
        return checkedArr.filter((item) => item !== id);
      } else {
        return [...prev, id];
      }
    });
  };
  /**
   * check checkedArr length to enable edit and delete button
   * case length != 1: disable edit button
   * case length > 0 : enable delete button
   */
  useEffect(() => {
    if (checkedArr.length !== 1) {
      setDisableEdit(true);
    } else {
      setDisableEdit(false);
    }

    if (checkedArr.length > 0) {
      setDisableDelete(false);
    } else {
      setDisableDelete(true);
    }
  }, [checkedArr.length]);

  /**
   * when click check all checkbox in table header
   * @param {*} e : event
   * case checked: get all ID of each obj in array and pass into checkedArr, enable delete button
   * case not checked: reset checkedArr into empty array
   */
  const handleCheckAll = (e) => {
    if (e.target.checked) {
      const result = state.displayArr.map((item) => item.Id);
      setCheckedArr(result);
      setDisableDelete(false);
      setCheckedAll(true);
    } else {
      setCheckedArr([]);
      setCheckedAll(false);
      setDisableDelete(true);
    }
  };

  useEffect(() => {
    if (state.displayArr?.length !== checkedArr.length) {
      setCheckedAll(false);
    } else {
      setCheckedAll(true);
    }
    if (checkedArr.length === 0) {
      setCheckedAll(false);
    }
  }, [checkedArr.length, state.displayArr?.length]);

  useEffect(() => {
    if (mode === "add") {
      setCheckedArr([]);
    }
  }, [mode]);

  const buttonArr = [
    {
      id: 1,
      name: "Edit",
      color: "light",
      disable: disableEdit,
      onClick: handleEditClick,
    },
    {
      id: 2,
      name: "Delete",
      color: "light",
      disable: disableDelete,
      onClick: handleDeleteClick,
    },
    {
      id: 3,
      name: "Sync",
      color: "light",
      disable: false,
      onClick: handleSyncClick,
    },
    {
      id: 4,
      name: "Delete All",
      color: "light",
      disable: false,
      onClick: handleDeleteAllClick,
    },
  ];

  return (
    <div className="tbContainer">
      <div className="tb-buttons">
        {buttonArr.map((btn) => {
          return (
            <Button
              key={btn.id}
              name={btn.name}
              color={btn.color}
              onClick={btn.onClick}
              disable={btn.disable}
            />
          );
        })}
      </div>

      <div className="tb-content">
        <table>
          <thead>
            <tr>
              {cols?.map((headerItem, index) => (
                <th key={index}>
                  {headerItem.title({
                    handleCheckAll,
                    checkedAll,
                  })}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {data?.map((item, index) => (
              <tr key={index}>
                {cols?.map((col, key) => (
                  <td key={key}>
                    {col.render({
                      rowData: item,
                      handleChecked,
                      checkedArr,
                      index,
                    })}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Table;
