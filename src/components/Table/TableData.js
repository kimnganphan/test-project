export const tableConstant = () => {
  return [
    {
      title: ({ handleCheckAll, checkedAll }) => {
        return <input checked={checkedAll} type="checkbox" onChange={(e) => handleCheckAll(e)} />;
      },
      render: ({ rowData, handleChecked, checkedArr }) => {
        return (
          <input
            type="checkbox"
            value={rowData.Id}
            checked={checkedArr?.includes(rowData.Id)}
            onChange={(e) => handleChecked(e, rowData.Id)}
          />
        );
      },
    },
    {
      title: () => {
        return "No.";
      },
      render: ({ index }) => {
        return <span>{index + 1}</span>;
      },
    },

    {
      title: () => {
        return "MaDinhDanh";
      },

      render: ({ rowData }) => {
        return <span>{rowData.MaDinhDanh}</span>;
      },
    },
    {
      title: () => {
        return "MaQuanLyQuocGia";
      },
      render: ({ rowData }) => {
        return <span>{rowData.MaQuanLy_QuocGia}</span>;
      },
    },
    {
      title: () => {
        return "Ten";
      },
      render: ({ rowData }) => {
        return <span>{rowData.Ten}</span>;
      },
    },
  ];
};
