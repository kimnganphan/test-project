import React from "react";
import "./Button.scss";
const Button = ({ name, disable, color, onClick }) => {
  return (
    <button className={color} disabled={disable} onClick={onClick}>
      {name}
    </button>
  );
};

export default Button;
