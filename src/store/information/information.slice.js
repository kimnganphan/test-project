import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  inputChange: {},
  displayArr: [],
  apiReceiveArr: [],
  addNewArr: [],
};

export const infoSlice = createSlice({
  name: "information",
  initialState,
  reducers: {
    getInfoFromApi: (state, action) => {
      state.apiReceiveArr = action.payload;
    },
    setInputChange: (state, action) => {
      state.inputChange = action.payload;
    },
    addInfo: (state, action) => {
      state.addNewArr.push(action.payload);
    },
    mergeArr: (state) => {
      state.displayArr = [...state.addNewArr, ...state.apiReceiveArr];
    },
    replaceInfo: (state, action) => {
      state.displayArr.splice(
        state.displayArr.findIndex((x) => x.Id === action.payload.Id),
        1,
        action.payload
      );
    },
    overrideInfo: (state, action) => {
      state.addNewArr.splice(
        state.addNewArr.findIndex((inf) => inf.Id === action.payload),
        1
      );
    },
    deleteAllInfo: (state) => {
      state.apiReceiveArr = [];
      state.addNewArr = [];
      state.displayArr = [];
      state.inputChange = {};
    },
    getSingleInfo: (state, action) => {
      state.inputChange = state.displayArr.find((inf) => {
        return inf.Id === action.payload;
      });
    },
    editInfo: (state, action) => {
      state.displayArr[
        state.displayArr.findIndex((inf) => inf.Id === action.payload.id)
      ] = action.payload.info;
    },

    deleteInfo: (state, action) => {
      state.displayArr.splice(
        state.displayArr.findIndex((inf) => inf.Id === action.payload),
        1
      );
      state.addNewArr.splice(
        state.addNewArr.findIndex((inf) => inf.Id === action.payload),
        1
      );
      state.apiReceiveArr.splice(
        state.apiReceiveArr.findIndex((inf) => inf.Id === action.payload),
        1
      );
    },
  },
});

export const {
  getInfoFromApi,
  deleteAllInfo,
  addInfo,
  getSingleInfo,
  editInfo,
  setInputChange,
  deleteInfo,
  replaceInfo,
  mergeArr,
  overrideInfo,
  deletetest,
} = infoSlice.actions;

export default infoSlice.reducer;
