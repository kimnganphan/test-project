export const errorInp = (inp) => {
  return !inp || inp?.toString().trim() === "";
};


export const generateRandomId = () => {
  return Math.floor((Math.random()*1)*9999)
}