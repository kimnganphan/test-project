import Form from "./components/Form/Form";
import Table from "./components/Table/Table";
import "./App.scss";
import { useEffect, useState } from "react";
import { tableConstant } from "./components/Table/TableData";
import { useDispatch, useSelector } from "react-redux";
import { mergeArr } from "./store/information/information.slice";

function App() {
  const [mode, setMode] = useState("add");

  const state = useSelector((state) => state.info);
  const dispatch = useDispatch()
  useEffect(()=>{
    dispatch(mergeArr())
  },[dispatch, state.addNewArr, state.apiReceiveArr])

  return (
    <div className="container">
      <Form mode={mode} setMode={setMode} />
      <Table cols={tableConstant()} setMode={setMode} data={state.displayArr} mode={mode}/>
    </div>
  );
}

export default App;
